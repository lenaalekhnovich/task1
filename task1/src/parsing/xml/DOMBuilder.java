package parsing.xml;

import figure.Line;
import figure.Point;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;


/**
 * Created by Босс on 11.12.2016.
 */
public class DOMBuilder {

    private Point[] points;
    private Line lineFirst;
    private Line lineSecond;
    private Line lineThird;

    static Logger logger = Logger.getLogger(DOMBuilder.class);

    public DOMBuilder(){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setIgnoringComments(true);
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error("negative argument: ", e);
        }
        Document document = null;
        try {
            document = builder.parse("src/parsing/xml/lines.xml");
        } catch (SAXException e) {
            logger.error("negative argument: ", e);
        } catch (IOException e) {
            logger.error("negative argument: ", e);
        }
        points = new Point[6];
        int size = 0;
        Element lines = document.getDocumentElement();
        if ("lines".equals(lines.getTagName())) {
            NodeList line= lines.getElementsByTagName("line");
            for (int i = 0; i < line.getLength(); ++i) {
                Element lineElement = (Element) line.item(i);
                NodeList linePoints = lineElement.getElementsByTagName("*");
                for (int j = 0; j < linePoints.getLength(); ++j) {
                    Element element = (Element) linePoints.item(j);
                    if ("x1".equals(element.getTagName())) {
                        points[size] = new Point();
                        points[size].setX(Integer.parseInt(element.getTextContent()));
                    } else if ("y1".equals(element.getTagName())) {
                        points[size++].setY(Integer.parseInt(element.getTextContent()));
                    } else if("x2".equals(element.getTagName())) {
                        points[size] = new Point();
                        points[size].setX(Integer.parseInt(element.getTextContent()));
                    } else if ("y2".equals(element.getTagName())) {
                        points[size++].setY(Integer.parseInt(element.getTextContent()));
                    }
                }
            }
        }
        lineFirst = new Line(points[0], points[1]);
        lineSecond = new Line(points[2], points[3]);
        lineThird = new Line(points[4], points[5]);    }

    public Line getLineFirst() {
        return lineFirst;
    }

    public Line getLineSecond() {
        return lineSecond;
    }

    public Line getLineThird() {
        return lineThird;
    }
}

