package figure;

/**
 * Created by Босс on 10.12.2016.
 */
public class Point {

    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Point point = (Point) obj;
        if (this.x != point.x) {
            return false;
        }
        if (this.y != point.y)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 37;
        hash = hash * 17 + x;
        hash = hash * 17 + y;
        return hash;
    }
}
