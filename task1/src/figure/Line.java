package figure;

/**
 * Created by Босс on 10.12.2016.
 */
public class Line {

    private Point pointFirst;
    private Point pointSecond;

    public Line(Point pointFirst, Point pointSecond) {
        this.pointFirst = pointFirst;
        this.pointSecond = pointSecond;
    }

    public Point getPointFirst() {
        return pointFirst;
    }

    public Point getPointSecond() {
        return pointSecond;
    }
}
