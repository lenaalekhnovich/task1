package calculation;

import figure.Line;

import static java.lang.Math.*;


/**
 * Created by Босс on 10.12.2016.
 */
public class ArithmeticOperation {


    public int getCoefficientA(Line line){
        return line.getPointFirst().getY() - line.getPointSecond().getY();
    }

    public int getCoefficientB(Line line){
        return line.getPointSecond().getX() - line.getPointFirst().getX();
    }

    public int getCoefficientC(Line line){
        return line.getPointFirst().getX()*line.getPointSecond().getY() -
                line.getPointSecond().getX()*line.getPointFirst().getY();
    }

    public int getCoefficientParallel(Line line){
        return Math.abs(getCoefficientA(line) * getCoefficientB(line));
    }

    public double calculateAngle(Line lineFirst, Line lineSecond){
        double angle;
        double functionCosAngle = (getCoefficientA(lineFirst) * getCoefficientB(lineSecond) +
                getCoefficientB(lineFirst) * getCoefficientB(lineSecond)) /
                Math.sqrt(pow(getCoefficientA(lineFirst),2) + pow(getCoefficientB(lineFirst),2)) *
                Math.sqrt(pow(getCoefficientA(lineSecond),2) + pow(getCoefficientB(lineSecond),2));
        angle = Math.toDegrees(acos(functionCosAngle));
        return angle;
    }

    public double calculateDistance(Line lineFirst, Line lineSecond){
        double distance = Math.abs(getCoefficientC(lineSecond) - getCoefficientC(lineFirst)) /
                Math.sqrt(pow(getCoefficientA(lineFirst),2) + pow(getCoefficientB(lineFirst),2));
        return distance;
    }

    public boolean isParallel(Line lineFirst, Line lineSecond){
        if(getCoefficientParallel(lineFirst) == getCoefficientParallel(lineSecond)){
            return true;
        }
        return false;
    }

    public Boolean isCrossingAxis(Line line) {
        if (getCoefficientA(line) == 0 || getCoefficientB(line) == 0) {
            return true;
        }
        return null;
    }
}
