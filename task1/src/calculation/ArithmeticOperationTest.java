package calculation;

import figure.Point;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import parsing.xml.DOMBuilder;

/**
 * Created by Босс on 11.12.2016.
 */
public class ArithmeticOperationTest {

    private static ArithmeticOperation arithmeticOperation;
    private static DOMBuilder builder;

    @BeforeClass
    public static void initArithmeticOperation(){
        arithmeticOperation = new ArithmeticOperation();
        builder = new DOMBuilder();
    }

    @Test
    public void parallelLinesTest(){
        boolean result = arithmeticOperation.isParallel(builder.getLineFirst(), builder.getLineSecond());
        Assert.assertTrue(result);
    }

    @Test
    public void crossingAxisTest(){
        boolean result = arithmeticOperation.isCrossingAxis(builder.getLineFirst());
        Assert.assertNotNull(result);
    }

    @Test(expected = AssertionError.class)
    public void createLineTest() throws AssertionError{
        Point pointFirst = builder.getLineThird().getPointFirst();
        Point pointSecond = builder.getLineThird().getPointSecond();
        Assert.assertNotEquals(pointFirst, pointSecond);
    }

    @Test
    public void calculateDistanceTest(){
        double result = arithmeticOperation.calculateDistance(builder.getLineFirst(),builder.getLineSecond());
        Assert.assertNotEquals(result,0);
    }

    @Test
    public void calculateAngleTest(){
        double result = arithmeticOperation.calculateAngle(builder.getLineFirst(),builder.getLineSecond());
        Assert.assertNotEquals(result, 360);
    }
}
